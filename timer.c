#include <linux/module.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/time.h>
#include <linux/sched.h>
#include <linux/fs.h> 
#include <linux/uaccess.h> 
 
MODULE_LICENSE("GPL");

#define TIMER_PERIOD	(60*HZ)
#define TIME_FORMAT	"hh:mm"
#define UTC3		(3*60*60)
#define FILENAME 	"/tmp/current_time"
	
static struct timer_list watch_timer;

// Function puts current time in hh:mm format in file.
static int timestamp(int hh, int mm)
{
	static const char* filename = FILENAME;
	static struct file *f; 
	static char buff[] = TIME_FORMAT;
	
 	loff_t offset = 0; 
 	mm_segment_t fs; 
   	
	// Forming timestamp data to write in file 
	sprintf(buff, "%02d:%02d", hh, mm);
 	
	f = filp_open(filename, O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR); 
 	
	if(IS_ERR(f)) 
	{ 
		printk(KERN_ERR "file open failed: %s\n", filename ); 
      		return -ENOENT; 
   	} 

	// Write in file
   	fs = get_fs(); 
   	set_fs(get_ds()); 
   	if(vfs_write( f, buff, strlen( buff ), &offset) != strlen(buff)) 
	{ 
        	printk(KERN_ERR "failed to write\n"); 
        	return -EIO; 
   	} 
   	set_fs(fs); 
   	
	filp_close(f, NULL); 
   	return 0; 
}

// Callback function for watch-timer. 
// Puts time in file and redefine timer.
void timer_callback( unsigned long data )
{
	int ret;
	static struct timeval tv;
	static struct tm t;

	// Get time and write it in file	
	do_gettimeofday(&tv);
	time_to_tm(tv.tv_sec, UTC3, &t);
	// Ignore retval
  	timestamp(t.tm_hour, t.tm_min);  
  	
	// Modify timer
	ret = mod_timer( &watch_timer, jiffies + TIMER_PERIOD );
	if(ret) printk(KERN_ERR "Error mod timer\n");
}
 
int watch_init( void )
{
	int ret;
	setup_timer( &watch_timer, timer_callback, 0 );
	
	// Call rigth now
 	ret = mod_timer( &watch_timer, jiffies);
	if (ret) printk(KERN_ERR "Error mod_timer\n");
 
	return 0;
}
 
void watch_exit( void )
{
	del_timer( &watch_timer );
}

module_init(watch_init);
module_exit(watch_exit);
